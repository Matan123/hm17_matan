#pragma once

#include "CryptoDevice.h"
#include <hex.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <string>
#include <thread>
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <md5.h>

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 512            
#define IP_ADDRESS "127.0.0.1"
#define DEFAULT_PORT "8202"
#define PASSWORD "DC647EB65E6711E155375218212B3964"
#define NAME "Matan"
struct client_type
{
	SOCKET socket;
	int id;
	char received_message[DEFAULT_BUFLEN];
};

class Client
{
public:
	Client();
	~Client();
	int main();

private:
	int process_client(client_type  &new_client);
};

